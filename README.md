*************************
*** 4chan95 CSS Style ***
*************************

A CSS Style for 4chan that resembles the Windows95 User Interface.

There are two css files:

1. catalog.css  For styling the catalog page
2. 4chan95.css  For styling the thread page


To try it:
* It is recommended that you disable antialiasing (cleartype etc.)
* The style uses the Microsoft Sans Serif font.
* Works only with vanilla 4chan. Anyone wihsing to adapt it for other chans
  please feel free to do so.


Instructions:
==============
On any 4chan catalog page (e.g. http://boards.4chan.org/g/catalog) click on
Settings and paste the contents of the catalog.css in the "Custom CSS" box

On any 4chan thread click on "Settings" then click on "Miscellaneous"  and then
enable the "Custom CSS" option. The click on the "Edit" link next to the "Custom
CSS" option and paste the contents of the file "4chan95.css" and click "Save CSS"
and then "Save Settings".




